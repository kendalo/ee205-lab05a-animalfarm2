///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author @Kendal Oya <kendalo@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @27_Feb_2021
////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool newisNative, enum Color newColor, enum Gender newGender ) {
   isNative=newisNative;
   gender = newGender;         
   species = "Fistularia chinensis";    
   scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
   favoriteTemperature= 80.6;       
}

void Nunu::printInfo(){
   cout << "Nunu" << endl;
   cout << "   Is native = [" << boolalpha << isNative << "]" << endl;
   Fish::printInfo();
}
}

